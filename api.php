<?php
class EPayArcaApi
{
    public static function epayaRegisterOrder($amount)
    {
        $errors = "";
        $newURL = "";
        $result = array(); 
        
        $ordnum = date('dmy').EPayArcaApi::epayGetCounter();

        $orderdesc = "Payment for order number: ".$ordnum;

        $data = array('MERCHANTNUMBER' => variable_get('epayarca_gateway_merchant_number'),
          'ORDERNUMBER' => $ordnum,
          'AMOUNT' => $amount*100,
          'BACKURL' => variable_get('epayarca_gateway_backurl'),
          '$ORDERDESCRIPTION' => $orderdesc,
          'LANGUAGE' => 'EN',
          'DEPOSITFLAG' => '1',
          'MODE' => '1',
          'MERCHANTPASSWD' => variable_get('epayarca_gateway_password'));

        $data_string = http_build_query($data);
        $url = "https://epay.arca.am/svpg/Merchant2Rbs";
        $curl_result = $curl_err = '';
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, 443);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 4);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $curl_result = @curl_exec($ch);
        $curl_err = curl_error($ch);

        curl_close($ch);

        if ($curl_err != "") {
            $errors .= $curl_err.'<br/>';
        }
        else
        {
            if ($curl_result != "") {
                if(strpos(" ".$curl_result, "merchanNumber==null") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "orderNumber==null") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "orderNumber is not a number") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "amount==null") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "amount must have at least 3 chars")  > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "AMOUNT is not a number") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "depositFlag==null") > 0) {        
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "backUrl==null") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "orderDescription==null") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "merchantPasswd==null") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "merchantPasswdincorrect") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "wrong currency code") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                elseif (strpos(" ".$curl_result, "System error =") > 0) {
                    $errors .= $curl_result.'<br/>';
                }
                else
                {
                    $newURL = 'https://epay.arca.am/svpg/BPC/ACBA/AcceptPayment_acba_en.jsp?MDORDER='.$curl_result;
                }
            } 
            else 
            {
                $errors .= 'Result not found.<br/>';
            }
        } 

        $result = array('errors' => $errors, 'redirectUrl'=> $newURL, 'orderNumber'=>$curl_result);

        return $result;
    }

    public static function reciveDataFromAPI()
    {
        $errors = "";
        $trueResult = "true";

        if ($_SERVER['REQUEST_METHOD'] == "GET") {
            if (isset($_GET['MDORDER'])) {    
                $mdorder = $_GET['MDORDER'];
            } 
            else 
            {
                $errors .= 'MDORDER not found.';
            }
            
            if (isset($_GET['ANSWER'])) {
                    
                $answer = $_GET['ANSWER'];
                $pieces = explode(" ", $answer);

                $primarycode = explode("=", $pieces[0]);
                $secondarycode = explode("=", $pieces[1]);

                $primarycode = (string)$primarycode[1];
                $secondarycode = (string)$secondarycode[1];

                //echo $primarycode .'</br>';
                //echo $secondarycode.'</br>';

                //$answerXML = new SimpleXMLElement($answer);
                //$primarycode = ((string)$answerXML[0]['primaryRC']);
                //$secondarycode = ((string)$answerXML[0]['secondaryRC']);

                switch (true)
                { 
                case ($primarycode === '"0"' && $secondarycode === '"0"'): 
                    break; 
                default: $errors .= "Errors in reply. primarycode:".$primarycode .", secondarycode:". $secondarycode;         
                }

            }

            else 
            {
                $errors .= 'ANSWER not found.<br/>';
            } 
        } 
        else 
        {
            $errors .= 'No GET Variables';
        }

        if($errors != "") {
            return $errors;
        }
        else
        {    
            return $trueResult;
        }
    }

    private static function epayGetCounter() 
    {
        $countXML = simplexml_load_file(drupal_get_path('module', 'epayarca').'/count.xml') or die("Error: Cannot open count file for read");
        
        $fromXMLDate = trim(((string)$countXML[0]['date']));
        $fromXMLCount = trim(((string)$countXML[0]['count']));
        
        if ($fromXMLDate !== date('dmy')) {
            $countXML[0]['date'] = date('dmy');
            $countXML[0]['count'] = 0;
        }
        else
        {
            $fromXMLCount++;
          
            if ($fromXMLCount >= 999) {
                $fromXMLCount = 1;  
            }
          
            $countXML[0]['count'] = $fromXMLCount;
          
            if (strlen($fromXMLCount) == 1) {
                $fromXMLCount = '00'.$fromXMLCount;   
            }
            if (strlen($fromXMLCount) == 2) {
                $fromXMLCount = '0'.$fromXMLCount;    
            }
        }
        
        $countXML->asXML(drupal_get_path('module', 'epayarca').'/count.xml');
        
        return $fromXMLCount;
    }
}
